import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import "bootstrap"

Vue.prototype.$axios = axios



new Vue({
  render: h => h(App),
}).$mount('#app')
