# 使用到的框架
* axios 
* vue-codemirror(开源的在编辑器)
* 界面模仿leetcode 

# 项目预览地址
http://39.106.127.118:8081/

# 配套的后端：
https://gitee.com/china-bin/OnlineJavaComile.git

# 功能
* 中间可以拖拉工作区
* alt + / 进行代码提示
* 显示与隐藏控制台
* 执行代码： 只对第一个测试用例进行测试（用来让答题者，自校验代码）
* 提交: 用所有的测试用例测试代码（所有通过才是对的）

# 截图
![图片不存在](img/cut.png)

## 安装依赖包
```
npm install
```

### 本地运行
```
npm run serve
```

### 打包项目
```
npm run build
```




